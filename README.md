# Deprecated

Please use one or both of the following instead: 

TimeRocket nodejs common for assistant services
https://gitlab.com/timerocket/assistant/public/typescript/assistant-nodejs-common

Codex framework apps
https://gitlab.com/cryptexlabs/public/codex-nodejs-common