import { ClientInterface } from "@timerocket/data-model";
export declare class Client implements ClientInterface {
    readonly id: string;
    readonly version: string;
    readonly name: string;
    readonly variant: string;
    constructor(id: string, version: string, name: string, variant: string);
}
