"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceClient = void 0;
class ServiceClient {
    constructor(config) {
        this.config = config;
    }
    get id() {
        return this.config.clientId;
    }
    get name() {
        return this.config.appName;
    }
    get variant() {
        return this.config.environmentName;
    }
    get version() {
        return this.config.appVersion;
    }
    toJSON() {
        return {
            id: this.id,
            name: this.name,
            variant: this.variant,
            version: this.version,
        };
    }
}
exports.ServiceClient = ServiceClient;
