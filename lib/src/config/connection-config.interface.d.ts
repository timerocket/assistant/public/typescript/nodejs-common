import { HostConfigInterface } from "./host-config.interface";
export interface ConnectionConfigInterface extends HostConfigInterface {
    protocol: string;
    connectionUrl: string;
}
