"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ElasticsearchConfig = void 0;
class ElasticsearchConfig {
    constructor(url, pingIntervalSeconds) {
        this.url = url;
        this.pingIntervalSeconds = parseInt(pingIntervalSeconds, 10);
    }
}
exports.ElasticsearchConfig = ElasticsearchConfig;
