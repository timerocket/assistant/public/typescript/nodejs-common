"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HostConfig = void 0;
class HostConfig {
    constructor(host, port) {
        this.host = host;
        this.port = parseInt(port, 10);
    }
}
exports.HostConfig = HostConfig;
