import { JwtConfigInterface } from "./jwt-config.interface";
export declare class JwtConfig implements JwtConfigInterface {
    readonly aud: string;
    verify: boolean;
    constructor(jwtVerify: string, aud: string);
}
