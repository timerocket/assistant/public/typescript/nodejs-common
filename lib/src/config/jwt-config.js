"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtConfig = void 0;
class JwtConfig {
    constructor(jwtVerify, aud) {
        this.aud = aud;
        this.verify = jwtVerify === "true";
    }
}
exports.JwtConfig = JwtConfig;
