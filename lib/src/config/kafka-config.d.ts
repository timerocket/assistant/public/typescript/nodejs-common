import { EndpointConfig } from "./endpoint-config";
import { SASLMechanism } from "kafkajs";
import { KafkaConfigInterface } from "./kafka-config.interface";
export declare class KafkaConfig extends EndpointConfig implements KafkaConfigInterface {
    readonly host: string;
    readonly username: string;
    readonly password: string;
    readonly topicsConfigPath: string;
    readonly clientId: string;
    enabled: boolean;
    saslMechanism: SASLMechanism;
    constructor(enabled: string, host: string, port: string, username: string, password: string, saslMechanism: string, topicsConfigPath: string, clientId: string);
}
