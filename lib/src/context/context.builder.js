"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ContextBuilder_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContextBuilder = void 0;
const common_1 = require("@nestjs/common");
const data_model_1 = require("@timerocket/data-model");
const config_1 = require("../config");
const context_1 = require("./context");
const uuid_1 = require("uuid");
const util_1 = require("../util");
let ContextBuilder = ContextBuilder_1 = class ContextBuilder {
    constructor(logger, config, client, messageContext, i18nApi) {
        this.logger = logger;
        this.config = config;
        this.client = client;
        this.messageContext = messageContext;
        this.i18nApi = i18nApi;
    }
    build() {
        return new ContextBuilder_1(this.logger, this.config, Object.assign({}, this.client), Object.assign({}, this.messageContext), this.i18nApi);
    }
    setI18nApi(i18nApi) {
        this.i18nApi = i18nApi;
        return this;
    }
    setMetaFromHeaders(headers) {
        this.setMetaFromHeadersForNewMessage(headers);
        if (headers["x-client-version"]) {
            this.client.version = headers["x-client-version"];
        }
        if (headers["x-client-id"]) {
            this.client.id = headers["x-client-id"];
        }
        if (headers["x-client-name"]) {
            this.client.name = headers["x-client-name"];
        }
        if (headers["x-client-variant"]) {
            this.client.variant = headers["x-client-variant"];
        }
        return this;
    }
    setMetaFromHeadersForNewMessage(headers) {
        this.setCorrelationId(headers["x-correlation-id"]);
        this.setLocale(util_1.LocaleUtil.getLocaleFromHeaders(headers, this.getResult()));
        this.setStarted(headers["x-started"]);
        if (headers["x-context-category"]) {
            this.messageContext.category = headers["x-context-category"];
        }
        if (headers["x-context-id"]) {
            this.messageContext.id = headers["x-context-id"];
        }
        return this;
    }
    setMeta(meta) {
        this.setCorrelationId(meta.correlationId);
        this.setLocale(new data_model_1.Locale(meta.locale.language, meta.locale.country));
        this.setStarted(meta.time.started);
        return this;
    }
    setCorrelationId(correlationId) {
        this.correlationId = correlationId;
        return this;
    }
    setLocale(locale) {
        this.locale = new data_model_1.Locale(locale.language, locale.country);
        return this;
    }
    setStarted(date) {
        if (typeof date === "string") {
            this.started = new Date(date);
        }
        else {
            this.started = date;
        }
        return this;
    }
    _getStarted() {
        return this.started || new Date();
    }
    _getCorrelationId() {
        return this.correlationId || uuid_1.v4();
    }
    _getLocale() {
        return this.locale || new data_model_1.Locale("en", "US");
    }
    getResult() {
        return new context_1.Context(this._getCorrelationId(), this.logger, this.config, this.client, this._getLocale(), this.messageContext, this._getStarted(), this.i18nApi);
    }
};
ContextBuilder = ContextBuilder_1 = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [Object, config_1.DefaultConfig,
        data_model_1.ClientInterface, Object, Object])
], ContextBuilder);
exports.ContextBuilder = ContextBuilder;
