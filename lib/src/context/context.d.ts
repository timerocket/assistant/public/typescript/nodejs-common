/// <reference types="i18n" />
import { ClientInterface, LocaleI18nInterface, MessageContextInterface, MessageMetaInterface, MetaTypeEnum } from "@timerocket/data-model";
import { LoggerService } from "@nestjs/common";
import { DefaultConfig } from "../config";
export declare class Context {
    correlationId: string;
    config: DefaultConfig;
    client: ClientInterface;
    readonly locale: LocaleI18nInterface;
    readonly messageContext: MessageContextInterface;
    readonly i18nApi?: i18nAPI;
    readonly logger: LoggerService;
    started: Date;
    constructor(correlationId: string, logger: LoggerService, config: DefaultConfig, client: ClientInterface, locale: LocaleI18nInterface, messageContext: MessageContextInterface, started?: Date, i18nApi?: i18nAPI);
    getMessageMeta(type: MetaTypeEnum): MessageMetaInterface;
}
