"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Context = void 0;
const logger_1 = require("../logger");
const message_1 = require("../message");
class Context {
    constructor(correlationId, logger, config, client, locale, messageContext, started, i18nApi) {
        this.correlationId = correlationId;
        this.config = config;
        this.client = client;
        this.locale = locale;
        this.messageContext = messageContext;
        this.i18nApi = i18nApi;
        this.logger = new logger_1.ContextLogger(correlationId, config, client, logger);
        this.started = started || new Date(Date.now());
    }
    getMessageMeta(type) {
        return new message_1.MessageMeta(type, this.locale, this.config, this.correlationId, this.started);
    }
}
exports.Context = Context;
