"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventHandlerFactory = void 0;
class EventHandlerFactory {
    constructor(handlerKeyValueMap, handlerRegexList) {
        this.handlerKeyValueMap = handlerKeyValueMap;
        this.handlerRegexList = handlerRegexList;
    }
    getHandler(topic) {
        for (const item of this.handlerRegexList) {
            if (item.regexp.test(topic)) {
                return item.handler;
            }
        }
        if (!this.handlerKeyValueMap[topic]) {
            throw new Error(`Unknown topic: ${topic}`);
        }
        return this.handlerKeyValueMap[topic];
    }
}
exports.EventHandlerFactory = EventHandlerFactory;
