"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventHandlerRegex = void 0;
class EventHandlerRegex {
    constructor(handler, regexp) {
        this.handler = handler;
        this.regexp = regexp;
    }
}
exports.EventHandlerRegex = EventHandlerRegex;
