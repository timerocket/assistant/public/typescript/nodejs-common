"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocalesEnum = void 0;
var LocalesEnum;
(function (LocalesEnum) {
    LocalesEnum["UNKNOWN_ERROR"] = "unknown-error";
    LocalesEnum["I_AM_HEALTHY"] = "i-am-healthy";
    LocalesEnum["INVALID_REQUEST"] = "invalid-request";
    LocalesEnum["SUCCESS"] = "success";
    LocalesEnum["AN_ERROR_OCCURRED"] = "an-error-occurred";
    LocalesEnum["LANGUAGE_NOT_SUPPORTED"] = "language-not-supported";
})(LocalesEnum = exports.LocalesEnum || (exports.LocalesEnum = {}));
