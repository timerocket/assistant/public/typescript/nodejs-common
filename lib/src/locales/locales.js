"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.i18nData = void 0;
const { I18n } = require("i18n");
const packageJSON = require("../../package.json");
const i18nData = {};
exports.i18nData = i18nData;
const i18nInstance = new I18n();
i18nInstance.configure({
    locales: packageJSON.i18n.languages,
    directory: __dirname,
    register: i18nData,
});
