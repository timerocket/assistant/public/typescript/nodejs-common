import { InteractionInterface, MessageTypeEnum, PhoneNumberChannelEnum, ReplyDataInterface, SmsSourceInterface } from "@timerocket/data-model";
export declare class SmsReply implements ReplyDataInterface<SmsSourceInterface> {
    interaction: InteractionInterface;
    readonly source: SmsSourceInterface;
    readonly text: string;
    messageId: string;
    type: MessageTypeEnum;
    constructor(interaction: InteractionInterface, sentFrom: string, sentTo: string, channel: PhoneNumberChannelEnum, message: string);
}
