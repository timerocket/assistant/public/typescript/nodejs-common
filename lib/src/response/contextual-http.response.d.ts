/// <reference types="i18n" />
import { HttpResponse } from "./http-response";
import { HttpStatus } from "@nestjs/common";
import { JsonSerializableInterface } from "../message";
import { MessageInterface } from "@timerocket/data-model";
import { Context } from "../context";
export declare class ContextualHttpResponse<T> extends HttpResponse<T> implements JsonSerializableInterface<MessageInterface<T>> {
    private readonly _message;
    constructor(context: Context, status: HttpStatus, i18nData: i18nAPI, phraseKey: string);
    get data(): string;
}
