"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContextualHttpResponse = void 0;
const http_response_1 = require("./http-response");
const data_model_1 = require("@timerocket/data-model");
class ContextualHttpResponse extends http_response_1.HttpResponse {
    constructor(context, status, i18nData, phraseKey) {
        super(status, data_model_1.MetaTypeEnum.NA_HTTP_SIMPLE, context.locale, context.config, context.correlationId, context.started);
        this.meta.context = context.messageContext
            ? context.messageContext
            : this.meta.context;
        this._message = i18nData.__({
            phrase: phraseKey,
            locale: context.locale.i18n,
        });
    }
    get data() {
        return this._message;
    }
}
exports.ContextualHttpResponse = ContextualHttpResponse;
