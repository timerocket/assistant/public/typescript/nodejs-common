"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorMessage = void 0;
class ErrorMessage {
    constructor(locale, i18nData, developerPhrase, userPhrase, fallbackDeveloperText, fallbackUserText) {
        if (developerPhrase) {
            this._developerMessage = i18nData.__({
                phrase: developerPhrase,
                locale: locale.i18n,
            });
        }
        else {
            this._developerMessage = fallbackDeveloperText || "";
        }
        if (userPhrase) {
            this._userMessage = i18nData.__({
                phrase: userPhrase,
                locale: locale.i18n,
            });
        }
        else {
            this._userMessage = fallbackUserText || "";
        }
        if (!this._developerMessage) {
            throw new Error("Invalid error message. Developer message cannot both be empty");
        }
    }
    get developer() {
        return this._developerMessage;
    }
    get user() {
        return this._userMessage;
    }
    toJSON() {
        return {
            developer: this.developer,
            user: this.user,
        };
    }
}
exports.ErrorMessage = ErrorMessage;
