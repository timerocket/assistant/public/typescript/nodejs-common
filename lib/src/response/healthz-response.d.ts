import { HttpStatus } from "@nestjs/common";
import { LocaleI18nInterface, MessageInterface, MessageMetaInterface } from "@timerocket/data-model";
import { DefaultConfig } from "../config";
import { JsonSerializableInterface } from "../message";
export declare class HealthzResponse implements JsonSerializableInterface<MessageInterface<string>> {
    private locale;
    private readonly _phraseKey;
    readonly meta: MessageMetaInterface;
    constructor(status: HttpStatus, locale: LocaleI18nInterface, config: DefaultConfig);
    toJSON(): MessageInterface<string>;
    get data(): string;
}
