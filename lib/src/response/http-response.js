"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpResponse = void 0;
const http_response_meta_1 = require("./http-response-meta");
class HttpResponse {
    constructor(status, type, locale, config, correlationId, started) {
        this.status = status;
        this.type = type;
        this.meta = new http_response_meta_1.HttpResponseMeta(status, type, locale, config, correlationId, started);
    }
    toJSON() {
        return {
            meta: this.meta,
            data: this.data,
        };
    }
}
exports.HttpResponse = HttpResponse;
