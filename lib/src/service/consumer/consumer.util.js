"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConsumerUtil = void 0;
const util_1 = require("../../util");
class ConsumerUtil {
    constructor() { }
    static getTopics(topics) {
        const newTopics = [];
        for (const topic of topics) {
            const trimTopic = topic.trim();
            if (util_1.StringUtil.isRegexString(trimTopic)) {
                const regexString = trimTopic
                    .substr(0, 1)
                    .substr(trimTopic.length - 2, 1);
                newTopics.push(new RegExp(regexString));
            }
            else {
                newTopics.push(topic.trim());
            }
        }
        return newTopics;
    }
}
exports.ConsumerUtil = ConsumerUtil;
