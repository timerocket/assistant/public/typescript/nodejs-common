"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ElasticsearchHealthzService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../config");
const elasticsearch_1 = require("@nestjs/elasticsearch");
const healthz_1 = require("./healthz");
const time_util_1 = require("../util/time.util");
let ElasticsearchHealthzService = class ElasticsearchHealthzService {
    constructor(config, logger, elasticsearchService, healthzService) {
        this.config = config;
        this.logger = logger;
        this.elasticsearchService = elasticsearchService;
        this.healthzService = healthzService;
    }
    async start() {
        await this.healthzService.makeUnhealthy(healthz_1.HealthzComponentEnum.DATABASE_ELASTICSEARCH);
        this._ping();
        setInterval(() => {
            this._ping();
        }, this.config.elasticsearch.pingIntervalSeconds * 1000);
    }
    async waitForHealthy() {
        this.logger.log("Waiting for elasticsearch connection", "info");
        while (true) {
            try {
                await new Promise((resolve, reject) => {
                    this.elasticsearchService.ping({}, async (error) => {
                        if (!error) {
                            resolve(undefined);
                        }
                        else {
                            await time_util_1.TimeUtil.sleep(1000);
                            reject();
                        }
                    });
                });
                this.logger.log("Elasticsearch service connected", "info");
                await this.healthzService.makeHealthy(healthz_1.HealthzComponentEnum.DATABASE_ELASTICSEARCH);
                return;
            }
            catch (e) {
                this.logger.debug("Elasticsearch not available", "debug");
                await time_util_1.TimeUtil.sleep(1000);
            }
        }
    }
    _ping() {
        this.elasticsearchService.ping({}, async (error) => {
            if (error) {
                this.logger.error("Elasticsearch service could not be reached", "");
                await this.healthzService.makeUnhealthy(healthz_1.HealthzComponentEnum.DATABASE_ELASTICSEARCH);
            }
            else {
                this.logger.debug("Elasticsearch service was successfully pinged");
                await this.healthzService.makeHealthy(healthz_1.HealthzComponentEnum.DATABASE_ELASTICSEARCH);
            }
        });
    }
};
ElasticsearchHealthzService = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject("CONFIG")),
    __param(1, common_1.Inject("LOGGER")),
    __metadata("design:paramtypes", [config_1.DefaultConfig, Object, elasticsearch_1.ElasticsearchService,
        healthz_1.HealthzService])
], ElasticsearchHealthzService);
exports.ElasticsearchHealthzService = ElasticsearchHealthzService;
