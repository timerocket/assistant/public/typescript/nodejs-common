"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KafkaService = void 0;
const common_1 = require("@nestjs/common");
const kafkajs_1 = require("kafkajs");
const config_1 = require("../../config");
const kafka_logger_1 = require("./kafka.logger");
let KafkaService = class KafkaService {
    constructor(config, logger) {
        this.logger = logger;
        const kafkaClientConfig = {
            clientId: config.clientId,
            brokers: [config.kafka.endpoint],
            sasl: undefined,
            ssl: undefined,
        };
        if (config.kafka.username) {
            kafkaClientConfig.sasl = {
                mechanism: config.kafka.saslMechanism.toLowerCase(),
                username: config.kafka.username,
                password: config.kafka.password,
            };
            kafkaClientConfig.ssl = true;
        }
        const kafkaLogger = new kafka_logger_1.KafkaLogger(logger);
        this._kafka = new kafkajs_1.Kafka(Object.assign(Object.assign({}, kafkaClientConfig), { logCreator: (logLevel) => {
                return kafkaLogger.log.bind(kafkaLogger);
            } }));
    }
    async stopConsumer() {
        await this._kafkaConsumer.stop();
    }
    async connect() {
        if (this._kafkaConsumer) {
            await this._kafkaConsumer.connect();
        }
        if (this._kafkaProducer) {
            await this._kafkaProducer.connect();
        }
        this._kafkaAdmin = this._kafka.admin();
        await this._kafkaAdmin.connect();
    }
    async disconnect() {
        if (this._kafkaConsumer) {
            await this._kafkaConsumer.disconnect();
        }
        if (this._kafkaProducer) {
            await this._kafkaProducer.disconnect();
        }
    }
    async initializeConsumer(consumerGroup) {
        this._kafkaConsumer = this._kafka.consumer({ groupId: consumerGroup });
    }
    async initializeProducer() {
        this._kafkaProducer = this._kafka.producer();
    }
    async startConsumer(topics, callback) {
        const promises = [];
        for (const topic of topics) {
            promises.push(this._kafkaConsumer.subscribe({ topic, fromBeginning: false }));
        }
        try {
            await Promise.all(promises);
        }
        catch (e) {
            this.logger.error(e.message, e.trace);
        }
        await this._ensureTopicsExist(topics);
        await this._kafkaConsumer.run({
            autoCommit: true,
            eachMessage: async ({ topic, partition, message }) => {
                this.logger.log({
                    topic,
                    partition,
                    message: {
                        key: message.key,
                        value: message.value.toString(),
                    },
                });
                await callback(topic, message);
            },
        });
    }
    async _ensureTopicsExist(topics) {
        const stringTopics = [];
        for (const topic of topics) {
            if (typeof topic === "string") {
                stringTopics.push(topic);
            }
        }
        const currentTopics = await this._kafkaAdmin.listTopics();
        const createTopics = [];
        for (const topic of stringTopics) {
            if (!currentTopics.includes(topic)) {
                const createTopic = {
                    topic,
                };
                createTopics.push(createTopic);
            }
        }
        if (createTopics.length > 0) {
            await this._kafkaAdmin.createTopics({
                waitForLeaders: true,
                topics: createTopics,
            });
        }
    }
    async publish(topic, payload) {
        const json = JSON.stringify(payload);
        this.logger.debug(`Publishing kafka message: ${json} to topic ${topic}`);
        await this._kafkaProducer.send({
            topic,
            messages: [{ value: json }],
        });
    }
    async publishBulk(topic, payloads) {
        const messages = [];
        for (const payload of payloads) {
            messages.push({ value: JSON.stringify(payload), key: topic });
        }
        await this._kafkaProducer.send({
            topic,
            messages,
        });
    }
};
KafkaService = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject("CONFIG")),
    __param(1, common_1.Inject("LOGGER")),
    __metadata("design:paramtypes", [config_1.DefaultConfig, Object])
], KafkaService);
exports.KafkaService = KafkaService;
