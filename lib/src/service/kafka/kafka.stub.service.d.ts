import { LoggerService } from "@nestjs/common";
import { KafkaServiceInterface } from "./kafka-service.interface";
import { MessageInterface } from "@timerocket/data-model";
export declare class KafkaStubService implements KafkaServiceInterface {
    startProducer(): Promise<any>;
    send(payload: MessageInterface<any>, logger: LoggerService): Promise<any>;
}
