"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BroadcasterService = void 0;
class BroadcasterService {
    constructor() {
        this.subscribedClients = [];
    }
    broadcast(topic, message) {
        const broadCastMessage = JSON.stringify(message);
        for (const wsClient of this.subscribedClients) {
            wsClient.forwardFromTopic(topic, broadCastMessage);
        }
    }
    getAllTopicsForClients() {
        const topics = [];
        const topicToStrings = [];
        for (const client of this.subscribedClients) {
            const clientTopics = client.getTopics();
            for (const clientTopic of clientTopics) {
                if (!topicToStrings.includes(clientTopic.toString())) {
                    topics.push(clientTopic);
                    topicToStrings.push(clientTopic.toString());
                }
            }
        }
        return topics;
    }
}
exports.BroadcasterService = BroadcasterService;
