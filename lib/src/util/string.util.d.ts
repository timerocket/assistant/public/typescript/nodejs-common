export declare class StringUtil {
    private constructor();
    static isRegexString(str: string): boolean;
    static stringMatches(test: string, str: string): boolean;
}
