"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StringUtil = void 0;
class StringUtil {
    constructor() { }
    static isRegexString(str) {
        return str.substr(0, 1) === "/" && str.substr(str.length - 1, 1) === "/";
    }
    static stringMatches(test, str) {
        const testTrimmed = test.trim();
        const strTrimmed = str.trim();
        if (test.trim() === str.trim()) {
            return true;
        }
        if (this.isRegexString(testTrimmed)) {
            if (new RegExp(testTrimmed.substr(1, testTrimmed.length - 2)).test(strTrimmed)) {
                return true;
            }
        }
        return false;
    }
}
exports.StringUtil = StringUtil;
