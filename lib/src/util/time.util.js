"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimeUtil = void 0;
class TimeUtil {
    constructor() { }
    static sleep(milliseconds) {
        return new Promise((resolve) => {
            setTimeout(resolve, milliseconds);
        });
    }
}
exports.TimeUtil = TimeUtil;
