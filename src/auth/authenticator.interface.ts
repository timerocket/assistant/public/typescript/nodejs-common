export interface AuthenticatorInterface {
  authenticate(token: string): Promise<boolean>;
}
