# A note from the founder

The configuration module makes use of a lot of extension. Do not try this at home kids. Inheritance can kill you.

Professionals using inheritance are constantly monitored by emergency response teams.
