import { ConnectionConfigInterface } from "./connection-config.interface";
import { AuthenticatedEndpointConfigInterface } from "./authenticated-endpoint-config.interface";

export interface AuthenticatedConnectionConfigInterface
  extends ConnectionConfigInterface,
    AuthenticatedEndpointConfigInterface {}
