import { AuthenticatedEndpointConfigInterface } from "./authenticated-endpoint-config.interface";
import { SwitchConfigInterface } from "./switch-config.interface";
import { SASLMechanism } from "kafkajs";

export interface KafkaConfigInterface
  extends AuthenticatedEndpointConfigInterface,
    SwitchConfigInterface {
  saslMechanism: SASLMechanism;
  topicsConfigPath: string;
  clientId: string;
}
