import { SwitchConfigInterface } from "./switch-config.interface";
import { HostConfigInterface } from "./host-config.interface";
import { HostConfig } from "./host-config";
import { ConfigBooleanEnum } from "./config-boolean.enum";

export class MetricsHostConfig
  extends HostConfig
  implements SwitchConfigInterface, HostConfigInterface {
  public readonly enabled: boolean;

  constructor(public readonly host, port: string, enabled: string) {
    super(host, port);
    this.enabled = enabled === ConfigBooleanEnum.TRUE;
  }
}
