import {
  ClientInterface,
  LocaleI18nInterface,
  MessageContextInterface,
  MessageMetaInterface,
  MetaTypeEnum,
} from "@timerocket/data-model";
import { LoggerService } from "@nestjs/common";
import { DefaultConfig } from "../config";
import { ContextLogger } from "../logger";
import { MessageMeta } from "../message";

export class Context {
  public readonly logger: LoggerService;
  public started: Date;

  constructor(
    public correlationId: string,
    logger: LoggerService,
    public config: DefaultConfig,
    public client: ClientInterface,
    public readonly locale: LocaleI18nInterface,
    public readonly messageContext: MessageContextInterface,
    started?: Date,
    public readonly i18nApi?: i18nAPI
  ) {
    this.logger = new ContextLogger(correlationId, config, client, logger);
    this.started = started || new Date(Date.now());
  }

  getMessageMeta(type: MetaTypeEnum): MessageMetaInterface {
    return new MessageMeta(
      type,
      this.locale,
      this.config,
      this.correlationId,
      this.started
    );
  }
}
