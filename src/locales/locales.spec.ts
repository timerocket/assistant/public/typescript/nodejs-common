import { i18nData } from "./locales";

describe("locales", () => {
  it("should configure locales", () => {
    expect(
      i18nData.__({
        phrase: "i-am-healthy",
        locale: "eng-US",
      })
    ).toBe("I'm healthy");
  });
});
