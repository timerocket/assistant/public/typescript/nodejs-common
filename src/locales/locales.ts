// TODO remove https://github.com/DefinitelyTyped/DefinitelyTyped/issues/46871
// tslint:disable-next-line:no-var-requires
const { I18n } = require("i18n");
import * as packageJSON from "../../package.json";

const i18nData = {} as i18nAPI;

const i18nInstance = new I18n();

i18nInstance.configure({
  locales: packageJSON.i18n.languages,
  directory: __dirname,
  register: i18nData,
});

export { i18nData };
