import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from "@nestjs/common";
import { PhoneNumberChannelEnum } from "@timerocket/data-model";

@Injectable()
export class PhoneNumberChannelEnumPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    const validPhoneNumberChannels = Object.values(PhoneNumberChannelEnum);
    if (!validPhoneNumberChannels.includes(value)) {
      throw new BadRequestException(`Invalid phone number channel: ${value}`);
    }
    return value;
  }
}
