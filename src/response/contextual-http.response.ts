import { HttpResponse } from "./http-response";
import { HttpStatus } from "@nestjs/common";
import { JsonSerializableInterface } from "../message";
import { MessageInterface, MetaTypeEnum } from "@timerocket/data-model";
import { Context } from "../context";

export class ContextualHttpResponse<T>
  extends HttpResponse<T>
  implements JsonSerializableInterface<MessageInterface<T>> {
  private readonly _message: string;

  constructor(
    context: Context,
    status: HttpStatus,
    i18nData: i18nAPI,
    phraseKey: string
  ) {
    super(
      status,
      MetaTypeEnum.NA_HTTP_SIMPLE,
      context.locale,
      context.config,
      context.correlationId,
      context.started
    );
    this.meta.context = context.messageContext
      ? context.messageContext
      : this.meta.context;
    this._message = i18nData.__({
      phrase: phraseKey,
      locale: context.locale.i18n,
    });
  }

  public get data(): string {
    return this._message;
  }
}
