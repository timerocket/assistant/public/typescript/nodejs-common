import {
  ErrorMessageInterface,
  LocaleI18nInterface,
  LocaleInterface,
} from "@timerocket/data-model";
import { JsonSerializableInterface } from "../message";

export class ErrorMessage
  implements
    ErrorMessageInterface,
    JsonSerializableInterface<ErrorMessageInterface> {
  private readonly _developerMessage: string;
  private readonly _userMessage: string;

  constructor(
    locale: LocaleI18nInterface,
    i18nData: i18nAPI,
    developerPhrase: string,
    userPhrase: string,
    fallbackDeveloperText?: string,
    fallbackUserText?: string
  ) {
    if (developerPhrase) {
      this._developerMessage = i18nData.__({
        phrase: developerPhrase,
        locale: locale.i18n,
      });
    } else {
      this._developerMessage = fallbackDeveloperText || "";
    }
    if (userPhrase) {
      this._userMessage = i18nData.__({
        phrase: userPhrase,
        locale: locale.i18n,
      });
    } else {
      this._userMessage = fallbackUserText || "";
    }
    if (!this._developerMessage) {
      throw new Error(
        "Invalid error message. Developer message cannot both be empty"
      );
    }
  }

  public get developer() {
    return this._developerMessage;
  }

  public get user() {
    return this._userMessage;
  }

  public toJSON(): ErrorMessageInterface {
    return {
      developer: this.developer,
      user: this.user,
    };
  }
}
