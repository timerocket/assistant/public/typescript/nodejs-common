import { HttpStatus } from "@nestjs/common";
import {
  MessageInterface,
  MessageMetaInterface,
  MetaTypeEnum,
} from "@timerocket/data-model";
import { JsonSerializableInterface } from "../message";
import { Context } from "../context";
import { HttpResponseMeta } from "./http-response-meta";

export class RestResponse
  implements JsonSerializableInterface<MessageInterface<string>> {
  public readonly meta: MessageMetaInterface;

  constructor(
    context: Context,
    status: HttpStatus,
    type: MetaTypeEnum,
    public readonly data: any
  ) {
    this.meta = new HttpResponseMeta(
      status,
      type,
      context.locale,
      context.config,
      context.correlationId,
      context.started
    ) as MessageMetaInterface;
  }

  public toJSON(): MessageInterface<any> {
    return {
      meta: this.meta,
      data: this.data,
    };
  }
}
