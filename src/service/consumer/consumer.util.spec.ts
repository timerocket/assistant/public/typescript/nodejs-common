import { ConsumerUtil } from "./consumer.util";

describe("Consumer util", () => {
  describe("getTopics", () => {
    it("should get string topics from string topics", () => {
      const inputTopics = ["hello.world", "good.dog"];

      const outputTopics = ConsumerUtil.getTopics(inputTopics);

      expect(typeof outputTopics[0]).toBe("string");
      expect(typeof outputTopics[1]).toBe("string");
    });

    it("should get regex topics from strings that start and end with '/'", () => {
      const inputTopics = ["/hello\\.world/", "/good\\.dog/"];

      const outputTopics = ConsumerUtil.getTopics(inputTopics);

      expect(outputTopics[0]).toBeInstanceOf(RegExp);
      expect(outputTopics[1]).toBeInstanceOf(RegExp);

      expect((outputTopics[0] as RegExp).test("hello.world")).toBe(true);
      expect((outputTopics[1] as RegExp).test("good.dog")).toBe(true);
    });
  });
});
