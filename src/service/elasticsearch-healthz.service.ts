import { Inject, Injectable, LoggerService } from "@nestjs/common";
import { DefaultConfig } from "../config";
import { ElasticsearchService } from "@nestjs/elasticsearch";
import { HealthzComponentEnum, HealthzService } from "./healthz";
import { TimeUtil } from "../util/time.util";

@Injectable()
export class ElasticsearchHealthzService {
  constructor(
    @Inject("CONFIG") private readonly config: DefaultConfig,
    @Inject("LOGGER") private readonly logger: LoggerService,
    private readonly elasticsearchService: ElasticsearchService,
    private readonly healthzService: HealthzService
  ) {}

  async start() {
    await this.healthzService.makeUnhealthy(
      HealthzComponentEnum.DATABASE_ELASTICSEARCH
    );
    this._ping();
    setInterval(() => {
      this._ping();
    }, this.config.elasticsearch.pingIntervalSeconds * 1000);
  }

  async waitForHealthy(): Promise<void> {
    this.logger.log("Waiting for elasticsearch connection", "info");
    while (true) {
      try {
        await new Promise((resolve, reject) => {
          this.elasticsearchService.ping({}, async (error) => {
            if (!error) {
              resolve(undefined);
            } else {
              await TimeUtil.sleep(1000);
              reject();
            }
          });
        });

        this.logger.log("Elasticsearch service connected", "info");
        await this.healthzService.makeHealthy(
          HealthzComponentEnum.DATABASE_ELASTICSEARCH
        );
        return;
      } catch (e) {
        this.logger.debug("Elasticsearch not available", "debug");
        await TimeUtil.sleep(1000);
      }
    }
  }

  private _ping() {
    this.elasticsearchService.ping({}, async (error) => {
      if (error) {
        this.logger.error("Elasticsearch service could not be reached", "");
        await this.healthzService.makeUnhealthy(
          HealthzComponentEnum.DATABASE_ELASTICSEARCH
        );
      } else {
        this.logger.debug("Elasticsearch service was successfully pinged");
        await this.healthzService.makeHealthy(
          HealthzComponentEnum.DATABASE_ELASTICSEARCH
        );
      }
    });
  }
}
