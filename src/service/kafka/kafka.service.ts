import { Inject, Injectable, LoggerService } from "@nestjs/common";
import {
  Admin,
  Consumer,
  ITopicConfig,
  Kafka,
  Message,
  Producer,
  SASLMechanism,
} from "kafkajs";
import { DefaultConfig } from "../../config";
import { ConsumerServiceDelegateInterface } from "../consumer";
import { KafkaLogger } from "./kafka.logger";

@Injectable()
export class KafkaService implements ConsumerServiceDelegateInterface {
  private _kafka: Kafka;
  private _kafkaConsumer: Consumer;
  private _kafkaProducer: Producer;
  private _kafkaAdmin: Admin;

  constructor(
    @Inject("CONFIG") config: DefaultConfig,
    @Inject("LOGGER") private readonly logger: LoggerService
  ) {
    const kafkaClientConfig = {
      clientId: config.clientId,
      brokers: [config.kafka.endpoint],
      sasl: undefined,
      ssl: undefined,
    };

    if (config.kafka.username) {
      kafkaClientConfig.sasl = {
        mechanism: config.kafka.saslMechanism.toLowerCase() as SASLMechanism,
        username: config.kafka.username,
        password: config.kafka.password,
      };
      kafkaClientConfig.ssl = true;
    }

    const kafkaLogger = new KafkaLogger(logger);

    this._kafka = new Kafka({
      ...kafkaClientConfig,
      logCreator: (logLevel) => {
        return kafkaLogger.log.bind(kafkaLogger);
      },
    });
  }

  async stopConsumer(): Promise<void> {
    await this._kafkaConsumer.stop();
  }

  public async connect(): Promise<void> {
    if (this._kafkaConsumer) {
      await this._kafkaConsumer.connect();
    }
    if (this._kafkaProducer) {
      await this._kafkaProducer.connect();
    }
    this._kafkaAdmin = this._kafka.admin();
    await this._kafkaAdmin.connect();
  }

  public async disconnect(): Promise<void> {
    if (this._kafkaConsumer) {
      await this._kafkaConsumer.disconnect();
    }
    if (this._kafkaProducer) {
      await this._kafkaProducer.disconnect();
    }
  }

  public async initializeConsumer(consumerGroup: string): Promise<void> {
    this._kafkaConsumer = this._kafka.consumer({ groupId: consumerGroup });
  }

  public async initializeProducer(): Promise<void> {
    this._kafkaProducer = this._kafka.producer();
  }

  public async startConsumer(
    topics: (string | RegExp)[],
    callback: (topic: string, message: Message) => Promise<void>
  ): Promise<void> {
    const promises = [];
    for (const topic of topics) {
      promises.push(
        this._kafkaConsumer.subscribe({ topic, fromBeginning: false })
      );
    }
    try {
      await Promise.all(promises);
    } catch (e) {
      this.logger.error(e.message, e.trace);
    }

    await this._ensureTopicsExist(topics);

    await this._kafkaConsumer.run({
      autoCommit: true,
      eachMessage: async ({ topic, partition, message }) => {
        this.logger.log({
          topic,
          partition,
          message: {
            key: message.key,
            value: message.value.toString(),
          },
        });
        await callback(topic, message);
      },
    });
  }

  private async _ensureTopicsExist(topics: (string | RegExp)[]): Promise<void> {
    const stringTopics = [];
    for (const topic of topics) {
      if (typeof topic === "string") {
        stringTopics.push(topic);
      }
    }

    const currentTopics = await this._kafkaAdmin.listTopics();
    const createTopics: ITopicConfig[] = [];
    for (const topic of stringTopics) {
      if (!currentTopics.includes(topic)) {
        const createTopic: ITopicConfig = {
          topic,
        };
        createTopics.push(createTopic);
      }
    }

    if (createTopics.length > 0) {
      await this._kafkaAdmin.createTopics({
        waitForLeaders: true,
        topics: createTopics,
      });
    }
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async publish(topic: string, payload: any): Promise<void> {
    const json = JSON.stringify(payload);
    this.logger.debug(`Publishing kafka message: ${json} to topic ${topic}`);
    await this._kafkaProducer.send({
      topic,
      messages: [{ value: json }],
    });
  }

  public async publishBulk(topic: string, payloads: any[]): Promise<void> {
    const messages = [];

    for (const payload of payloads) {
      messages.push({ value: JSON.stringify(payload), key: topic });
    }

    await this._kafkaProducer.send({
      topic,
      messages,
    });
  }
}
