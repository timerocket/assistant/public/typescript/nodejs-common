/* eslint-disable @typescript-eslint/no-unused-vars,@typescript-eslint/explicit-module-boundary-types */
import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  WebSocketGateway,
  WebSocketServer,
} from "@nestjs/websockets";
import { HttpStatus, Inject, LoggerService } from "@nestjs/common";
import { DefaultConfig } from "../../config";
import { AuthenticatorInterface, TopicAuthorizorInterface } from "../../auth";
import { SocketSubscribedClient } from "./socket-subscribed-client";
import { BroadcasterService } from "./broadcaster.service";
import { WebsocketConsumerService } from "../consumer";
import {
  Locale,
  MessageInterface,
  MetaTypeEnum,
  WebsocketSubscriptionsUpdatedInterface,
} from "@timerocket/data-model";
import { ErrorHttpResponse, SimpleHttpResponse } from "../../response";
import { ContextBuilder } from "../../context";
import { i18nData } from "../../locales/locales";
import { LocalesEnum } from "../../locales/enum";

@WebSocketGateway()
export class WebsocketGateway
  implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {
  @WebSocketServer() private server: any;

  private readonly subscribedTopics: string[];

  constructor(
    private readonly consumerService: WebsocketConsumerService,
    @Inject("CONFIG") private readonly config: DefaultConfig,
    @Inject("TOPIC_AUTHORIZORS")
    private readonly authorizors: TopicAuthorizorInterface[],
    @Inject("LOGGER") private readonly logger: LoggerService,
    @Inject("BROADCASTER")
    private readonly broadcasterService: BroadcasterService,
    @Inject("CONTEXT_BUILDER") private readonly contextBuilder: ContextBuilder,
    @Inject("AUTHENTICATOR")
    private readonly authenticator: AuthenticatorInterface
  ) {
    this.subscribedTopics = [];
  }

  public async afterInit() {
    await this.consumerService.init();
  }

  public async handleConnection(client: WebSocket) {
    const token = client.protocol;

    if (!(await this.authenticator.authenticate(token))) {
      this.logger.error(`Unauthorized ${HttpStatus.UNAUTHORIZED}`);
      client.close(108, `Unauthorized ${HttpStatus.UNAUTHORIZED}`);
    }

    const subscribedClient = new SocketSubscribedClient(
      token,
      client,
      this.authorizors
    );

    this.logger.debug(`Client connected`);

    this.broadcasterService.subscribedClients.push(subscribedClient);

    client.onmessage = (event: MessageEvent) => {
      this._onMessage(event, client, subscribedClient);
    };
  }

  public async _onMessage(
    event: MessageEvent,
    client: WebSocket,
    subscribedClient: SocketSubscribedClient
  ) {
    let message: MessageInterface<WebsocketSubscriptionsUpdatedInterface>;
    const now = new Date();
    try {
      this.logger.debug(`Received message: ${event.data}`);
      let context;

      if (!event.data) {
        context = this.contextBuilder.build().getResult();

        const userErrorMessage = i18nData.__({
          phrase: "an-error-occurred",
          locale: context.locale.i18n,
        });
        const developerMessage = `Message body is empty`;
        this.logger.error(developerMessage);

        const errorMessage = new ErrorHttpResponse(
          HttpStatus.BAD_REQUEST,
          context.locale,
          { user: userErrorMessage, developer: developerMessage },
          new Error().stack,
          this.config,
          message.meta.correlationId,
          now
        );

        client.send(JSON.stringify(errorMessage));
        return;
      }

      message = JSON.parse(
        event.data
      ) as MessageInterface<WebsocketSubscriptionsUpdatedInterface>;

      context = this.contextBuilder.build().setMeta(message.meta).getResult();

      if (
        message.meta?.type === MetaTypeEnum.NA_WEBSOCKET_SUBSCRIPTIONS_UPDATED
      ) {
        await subscribedClient.subscribeToTopics(message.data.subscriptions);

        await this._subscribeTopics(
          this.broadcasterService.getAllTopicsForClients()
        );

        const successMessage = new SimpleHttpResponse(
          context,
          HttpStatus.ACCEPTED,
          LocalesEnum.SUCCESS
        );

        client.send(JSON.stringify(successMessage));
      } else {
        const userErrorMessage = i18nData.__({
          phrase: "an-error-occurred",
          locale: context.locale.i18n,
        });
        const developerMessage = `Unknown message type ${message.meta.type}`;
        this.logger.error(developerMessage);

        const errorMessage = new ErrorHttpResponse(
          HttpStatus.BAD_REQUEST,
          context.locale,
          { user: userErrorMessage, developer: developerMessage },
          new Error().stack,
          this.config,
          message.meta.correlationId,
          now
        );

        client.send(JSON.stringify(errorMessage));
      }
    } catch (e) {
      const locale = new Locale("en", "US");
      const userMessage = i18nData.__({
        phrase: "unknown-error",
        locale: locale.i18n,
      });
      const developerErrorMessage = e.message;

      this.logger.error(developerErrorMessage, e.trace);

      const errorMessage = new ErrorHttpResponse(
        HttpStatus.BAD_REQUEST,
        locale,
        { user: userMessage, developer: developerErrorMessage },
        new Error().stack,
        this.config,
        message.meta.correlationId,
        now
      );

      client.send(JSON.stringify(errorMessage));
    }
  }

  private async _subscribeTopics(topics) {
    let didChangeSubscriptions = false;
    for (const subscription of topics) {
      if (!this.subscribedTopics.includes(subscription)) {
        didChangeSubscriptions = true;
        this.subscribedTopics.push(subscription);
      }
    }
    if (didChangeSubscriptions) {
      await this.consumerService.restart(this.subscribedTopics);
    }
  }

  public handleDisconnect(client) {
    for (let i = 0; i < this.broadcasterService.subscribedClients.length; i++) {
      if (this.broadcasterService.subscribedClients[i] === client) {
        this.broadcasterService.subscribedClients.splice(i, 1);
        break;
      }
    }
    this.broadcasterService.broadcast("disconnect", {});
  }
}
