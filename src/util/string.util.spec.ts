import { StringUtil } from "./string.util";

describe("StringUtil", () => {
  it("should be a regex string", () => {
    expect(
      StringUtil.isRegexString(
        "/reply\\.message\\.(app|sms|mms)\\.unassigned-interaction/"
      )
    ).toBe(true);
  });

  it("should not be a regex string", () => {
    expect(
      StringUtil.isRegexString(
        "/reply\\.message\\.(app|sms|mms)\\.unassigned-interaction"
      )
    ).toBe(false);
    expect(
      StringUtil.isRegexString(
        "reply\\.message\\.(app|sms|mms)\\.unassigned-interaction/"
      )
    ).toBe(false);
  });

  it("should match regex string", () => {
    expect(
      StringUtil.stringMatches(
        "/reply\\.message\\.(app|sms|mms)\\.unassigned-interaction/",
        "reply.message.mms.unassigned-interaction"
      )
    ).toBe(true);
    expect(
      StringUtil.stringMatches(
        "reply\\.message\\.(app|sms|mms)\\.unassigned-interaction/",
        "reply.message.asdf.unassigned-interaction"
      )
    ).toBe(false);
  });
});
